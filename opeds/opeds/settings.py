# Scrapy settings for opeds project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'opeds'

SPIDER_MODULES = ['opeds.spiders']
NEWSPIDER_MODULE = 'opeds.spiders'
DEPTH_LIMIT = 2
LOG_LEVEL = 'INFO'
COOKIES_ENABLED = True
AJAXCRAWL_ENABLED = True

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'opeds (+http://www.yourdomain.com)'
