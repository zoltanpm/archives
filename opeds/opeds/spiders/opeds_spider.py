import os
import sqlite3
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from lxml import html


class OpedsSpider(CrawlSpider):
    name = "opeds_spider"
    start_urls = [
        "http://www.nytimes.com/pages/opinion/",
        "http://www.latimes.com/opinion/op-ed/",
        "http://www.washingtonpost.com/opinions/",
        "http://www.chicagotribune.com/news/opinion/",
        "http://www.tampabay.com/opinion/",
        "http://www.usatoday.com/opinion/",
        "http://www.denverpost.com/opinion",
        "http://www.sfgate.com/opinion/",
        "http://www.dallasnews.com/opinion/",
        "http://www.startribune.com/opinion/",
        "http://www.inquirer.com/opinion",
        "http://www.ajc.com/s/opinion/",
        "http://www.suntimes.com/opinions/",
        "http://www.chron.com/opinion/",
        "http://www.freep.com/section/OPINION01/Editorials",
        "http://www.mercurynews.com/opinion",
        "http://www.nydailynews.com/opinion",
        "http://www.azcentral.com/viewpoints/",
        "http://www.cleveland.com/opinion/",
        "http://www.newsday.com/opinion/oped",
        "http://www.nj.com/opinion/",
        "http://www.bostonglobe.com/opinion",
        "http://www.staradvertiser.com/editorials/",
        "http://www.reviewjournal.com/opinion"


        # List for testing purposes only. Adjust/narrow content
        # selection to fit research criteria here; adjust content/link
        # depth in ../settings.py
    ]

    rules = (
        Rule(LinkExtractor(allow=(r'.*opinion.*|.*editorial.*|.*blog.*')),
                           callback='parse_it', follow=True),
    )

    def parse_it(self, response):
        """Parse URL content with lxml.

        Take Response object body and URL, and extract <p> elements. \
        raw_content reassembles lxml objects into a format that can be \
        passed to sqlite
        """
        data = response.body
        url = response.url
        into_lxml = html.fromstring(data)
        p_tags = into_lxml.xpath('//p')
        raw_content = ' '.join([p.text_content() for p in p_tags])
        add_to_db(raw_content, url)


def add_to_db(raw_content, url):
    """Add content to sqlite3 database.

    Database naming reflects my server--work-machine workflow. Adjust \
    accordingly. Adjust UNIQUE column to fit data (e.g., news_broad \
    search should not set URL to UNIQUE) to avoid capturing different \
    URL/ref=XYZ pages that contain the same text.
    """

    conn = sqlite3.connect(os.path.expanduser(
                           'opeds.db'
                           ))
    cur = conn.cursor()

    cur.execute('''CREATE TABLE if not exists opeds
                (id INTEGER PRIMARY KEY, web_content TEXT UNIQUE,
                URL TEXT, UTC DATE)''')

    cur.execute('''
              INSERT or IGNORE INTO opeds VALUES
              (?, ?, ?, DATETIME('NOW','-05:00'))''', (None, raw_content, url))

    conn.commit()
    conn.close()

    # delete from news_broad where UTC < datetime('now', '-10 day');
