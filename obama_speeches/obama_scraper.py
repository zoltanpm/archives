#!/usr/bin/env python3

import re
import requests
from bs4 import BeautifulSoup

header = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0',}
raw_data = requests.get('http://www.americanrhetoric.com/barackobamaspeeches.htm',
                        headers=header)
raw_soup = BeautifulSoup(raw_data.text, 'lxml')

link_list = []
for link in raw_soup.find_all('a', href=re.compile('.htm')):
    link_list.append("".join(['http://www.americanrhetoric.com/', link.get('href')]))

with open('obamaspeeches.txt', 'w') as f:
    for item in link_list:
        if 'obama' in item:
            temp_req = requests.get(item, headers=header)
            f.write(BeautifulSoup(temp_req.text, 'lxml').text)
f.close()
