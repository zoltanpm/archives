#!/usr/bin/env python3

import re
import requests
from bs4 import BeautifulSoup

Header = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0',}
Link_list = []

for x in range(0,39):
    link = "".join(['https://www.whitehouse.gov/briefing-room/speeches-and-remarks?page=',
                    str(x)])
    raw_data = requests.get(link, headers=Header)
    raw_soup = BeautifulSoup(raw_data.text, 'lxml')

    for link in raw_soup.find_all('a',
            href=re.compile('the\-press\-office/')):
        Link_list.append("".join(['https://www.whitehouse.gov/', link['href']]))

with open('trumpspeeches.txt', 'w') as f:
    for item in Link_list:
        if 'trump' in item:
            temp_req = requests.get(item, headers=Header)
            f.write(BeautifulSoup(temp_req.text, 'lxml').text)
f.close()
