# Scrapy settings for nyt project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'news_broad'

SPIDER_MODULES = ['news_broad.spiders']
NEWSPIDER_MODULE = 'news_broad.spiders'
DEPTH_LIMIT = 2
LOG_ENABLED = True
LOG_FILE = 'log.txt'
LOG_LEVEL = 'INFO'
COOKIES_ENABLED = True
AJAXCRAWL_ENABLED = True
# DEPTH_STATS = False

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'nyt (+http://www.yourdomain.com)'
