import os
import sqlite3
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
from lxml import html


class OpEdSpider(CrawlSpider):
    name = "news_broad"
    start_urls = [
        "http://www.latimes.com",
        "http://www.washingtonpost.com",
        "http://www.chicagotribune.com",
        "http://www.usatoday.com",
        "http://www.tampabay.com",
        "http://www.denverpost.com",
        "http://www.sfchronicle.com",
        "http://www.dallasnews.com",
        "http://www.startribune.com",
        "http://www.inquirer.com",
        "http://www.ajc.com"
        # List for testing purposes only. Adjust/narrow content
        # selection to fit research criteria here; adjust content/link
        # depth in ../settings.py
    ]

    rules = (
        Rule(LinkExtractor(allow=(r'[htp]+s*://[w]*\.'),
             deny_domains=('facebook.com')), callback='parse_it',
             follow=True),
    )

    def parse_it(self, response):
        """Parse URL content with lxml.

        Take Response object body and URL, and extract <p> elements. \
        raw_content reassembles lxml objects into a format that can be \
        passed to sqlite
        """
        data = response.body
        url = response.url
        into_lxml = html.fromstring(data)
        p_tags = into_lxml.xpath('//p')
        raw_content = ' '.join([p.text_content() for p in p_tags])
        add_to_db(raw_content, url)


def add_to_db(raw_content, url):
    """Add content to sqlite3 database.

    Database naming reflects my server--work-machine workflow. Adjust \
    accordingly. Adjust UNIQUE column to fit data (e.g., news_broad \
    search should not set URL to UNIQUE) to avoid capturing different \
    URL/ref=XYZ pages that contain the same text.
    """

    conn = sqlite3.connect(os.path.expanduser(
                           '~/projects/propagating_ideas/propagating_ideas.db'
                           ))
    cur = conn.cursor()

    cur.execute('''CREATE TABLE if not exists news_broad
                (id INTEGER PRIMARY KEY, web_content TEXT UNIQUE,
                URL TEXT, UTC DATE)''')

    cur.execute('''
              INSERT or IGNORE INTO news_broad VALUES
              (?, ?, ?, DATETIME('NOW'))''', (None, raw_content, url))

    cur.execute('''
                DELETE from news_broad
                where UTC < datetime('now', '-60 day')
                ''')

    conn.commit()
    conn.close()

    # delete from news_broad where UTC < datetime('now', '-10 day');
