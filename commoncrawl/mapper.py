#!/usr/bin/python

import sys
import re
# import warc
from os.path import expanduser
import gzip
import requests

match_word = re.compile(r'expert', re.IGNORECASE)
match_word2 = re.compile(r'experience', re.IGNORECASE)

for line in sys.stdin:
    for word in re.findall(match_word, line):
        print('{}\t{}'.format(match_word.pattern, 1))
    for word in re.findall(match_word2, line):
        print('{}\t{}'.format(match_word2.pattern, 1))
