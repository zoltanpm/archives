import pandas as pd


with open('data/NIH_abs.txt') as f:
    pylist = f.read().split('APPLICATION_ID')
   
idlist = pd.Series(pylist[1::2])
abslist = pd.Series(pylist[2::2])
   
pdlist = pd.concat([idlist, abslist], axis=1, keys = ['ID', 'Abstract'])
